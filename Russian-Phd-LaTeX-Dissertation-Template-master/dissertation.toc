\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\changetocdepth {2}
\babel@toc {russian}{}
\defcounter {refsection}{0}\relax 
\tocheader 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Введение}{4}{chapter*.2}%
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {1}Оформление различных элементов}{7}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Форматирование текста}{7}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.2}Ссылки}{7}{section.1.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.3}Формулы}{7}{section.1.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.1}Ненумерованные одиночные формулы}{8}{subsection.1.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.2}Ненумерованные многострочные формулы}{8}{subsection.1.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.3}Нумерованные формулы}{10}{subsection.1.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.4}Форматирование чисел и размерностей величин}{11}{subsection.1.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3.5}Заголовки с формулами: \(a^2 + b^2 = c^2\), \(\left \vert \textrm {{Im}}\Sigma \left ( \varepsilon \right )\right \vert \approx const\), \(\upsigma _{xx}^{(1)}\) }{14}{subsection.1.3.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.4}Рецензирование текста}{15}{section.1.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.5}Работа со списком сокращений и~условных обозначений}{15}{section.1.5}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {2}Длинное название главы, в которой мы смотрим на~примеры того, как будут верстаться изображения и~списки}{17}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Одиночное изображение}{17}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Длинное название параграфа, в котором мы узнаём как сделать две картинки с~общим номером и названием}{17}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Пример вёрстки списков}{19}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Традиции русского набора}{20}{section.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.1}Пробелы}{21}{subsection.2.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.2}Математические знаки и символы}{21}{subsection.2.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.3}Кавычки}{21}{subsection.2.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.4}Тире}{22}{subsection.2.4.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.5}Дефисы и переносы слов}{23}{subsection.2.4.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Текст из панграмм и формул}{23}{section.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\chapternumberline {3}Вёрстка таблиц}{27}{chapter.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Таблица обыкновенная}{27}{section.3.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Таблица с многострочными ячейками и примечанием}{28}{section.3.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Таблицы с форматированными числами}{28}{section.3.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Параграф \cyrdash {} два}{29}{section.3.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}Параграф с подпараграфами}{30}{section.3.5}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.1}Подпараграф \cyrdash {} один}{30}{subsection.3.5.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.2}Подпараграф \cyrdash {} два}{30}{subsection.3.5.2}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Заключение}{33}{chapter*.30}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Словарь терминов}{34}{chapter*.31}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Список литературы}{35}{section*.33}%
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {english}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\babel@toc {russian}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Список рисунков}{45}{section*.37}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{Список таблиц}{46}{section*.38}%
\defcounter {refsection}{0}\relax 
\contentsline {appendix}{\chapternumberline {\CYRA }Примеры вставки листингов программного кода}{47}{appendix.A}%
\defcounter {refsection}{0}\relax 
\contentsline {appendix}{\chapternumberline {\CYRB }Очень длинное название второго приложения, в~котором продемонстрирована работа с~длинными таблицами}{53}{appendix.B}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {\CYRB .1}Подраздел приложения}{53}{section.B.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {\CYRB .2}Ещё один подраздел приложения}{55}{section.B.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {\CYRB .3}Использование длинных таблиц с окружением \textit {longtabu}}{59}{section.B.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {\CYRB .4}Форматирование внутри таблиц}{62}{section.B.4}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {\CYRB .5}Стандартные префиксы ссылок}{64}{section.B.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {\CYRB .6}Очередной подраздел приложения}{65}{section.B.6}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {\CYRB .7}И ещё один подраздел приложения}{65}{section.B.7}%
\defcounter {refsection}{0}\relax 
\contentsline {appendix}{\chapternumberline {\CYRV }Чертёж детали}{66}{appendix.C}%
